import boto3
import json
import os
from datetime import datetime, timedelta
from classes.collector_setup import CollectorSetupClass

def cloudWatch(data, lock):

	# Instantiate Classes
	collector = CollectorSetupClass(data)
	logger = collector.logclass.setup()
	dataWriter = collector.data_writer

	try:
		# Check config variables
		collector.log("Loading configuration.")
		if 'metric' in collector.config:
			metricVar = collector.config['metric']
		else:
			raise Exception("Configuration error: metricVar")

		if 'nametag' in collector.config:
			nameTag = collector.config["nametag"]
		else:
			raise Exception("Configuration error: nameTag")

		if 'namespace' in collector.config:
			nameSpace = collector.config['namespace']
		else:
			raise Exception("Configuration error: nameSpace")

		if 'region-name' in collector.config:
			regionName = collector.config['region-name']
		else:
			raise Exception("Configuration error: regionName")

		if 'high-thresh' in collector.config:
			highThresh = int(collector.config['high-thresh'])
		else:
			raise Exception("Configuration error: highThresh")

		if 'low-thresh' in collector.config:
			lowThresh = int(collector.config['low-thresh'])
		else:
			raise Exception("Configuration error: lowThresh")

		# Prep boto3 - Low level client is not thread safe unless declared in the cron and pass the object down to the collector
		collector.log("Prepping ASG lookup query")

		# Create boto3 handle for querying AWS - use thread lock since client instantiation is not thread safe
		with lock:
			ec2_client = boto3.client(service_name='ec2', region_name=(os.getenv("AWS_REGION") or 'us-east-1'))
			cloudwatch_client = boto3.client(service_name='cloudwatch', region_name=regionName)

		collector.log(f'Sending request for describe_instances')
		response = ec2_client.describe_instances(
			Filters=[{'Name': 'tag:Name', 'Values': [nameTag]}, {'Name': 'instance-state-name', 'Values': ['running']}],
			MaxResults=5)

		collector.log("Saving response")
		collector.rawresponse(json.dumps(response, sort_keys=True, default=str))  # Avoids: 'datetime.datetime(2020, 6, 9, 20, 4, tzinfo=tzutc())'

		# Locate AutoScaling Group
		collector.log("Determining AutoScaling Group")
		asgName = None
		if (len(response['Reservations']) > 0) and (response['Reservations'][0]['Instances'][0]['Tags'] is not None):
			tags = response['Reservations'][0]['Instances'][0]['Tags']
			for tag in tags:
				if tag['Key'] == 'aws:autoscaling:groupName':
					asgName = tag['Value']
			collector.log("ASG Found: " + str(asgName))
		else:
			logger.info(collector.log("Couldn't find tag: " + str(nameTag)))
			dataWriter.saveCollectorResults(collector.rawData, 0, data)
			return

		# Create handle for Cloudwatch query
		collector.log("Prepping EC2 get_metric_statistics request")

		# Query CloudWatch
		response = cloudwatch_client.get_metric_statistics(
			Namespace=nameSpace,
			Period=300,
			StartTime=datetime.utcnow() - timedelta(seconds=600),
			EndTime=datetime.utcnow(),
			MetricName=metricVar,
			Statistics=['Average'], Unit='Percent',
			Dimensions=[{'Name': 'AutoScalingGroupName', 'Value': asgName}])

		collector.log(json.dumps(response, sort_keys=True, default=str))

		# Calculate status
		collector.log("Calculating status")

		dataPoint = 0
		datapoints = response['Datapoints']
		num_datapoints = len(response['Datapoints'])
		if 'Datapoints' in response:
			if num_datapoints > 1:
				for node in datapoints:
					dataPoint += int(node['Average'])
				dataPoint /= num_datapoints
			elif num_datapoints == 1:
				dataPoint = round(datapoints[0]['Average'])
			else:
				logger.info(collector.log("get_metric_statistics returned no results."))
				dataWriter.saveCollectorResults(collector.rawData, 0, data)
				return
		else:
			logger.info(collector.log("Datapoings not in response."))
			dataWriter.saveCollectorResults(collector.rawData, 0, data)
			return

		if (dataPoint > highThresh):
			logger.info(collector.log(f'{metricVar} exceeding threshold: {dataPoint}'))
			dataWriter.saveCollectorResults(collector.rawData, 0, data)
		elif (dataPoint < lowThresh):
			collector.log(f'{metricVar} below threshold: {dataPoint}')
			dataWriter.saveCollectorResults(collector.rawData, 100, data)
		else:
			collector.log(f'{metricVar} nearing threshold: {dataPoint}')
			dataWriter.saveCollectorResults(collector.rawData, 50, data)

	except Exception as e:
		logger.error("Exception", exc_info=True)
		collector.rawData['except'] = str(e)
		dataWriter.saveCollectorResults(collector.rawData, '-1', data)

	finally:
		return collector.cleanUp()
